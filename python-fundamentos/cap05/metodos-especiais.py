# exemplo metodo especial __init__ (inicializar um objeto)
# todos os metodos especiais utilizam __metodo__

# Criando a classe Livro
class Livro():
    def __init__(self, titulo, autor, paginas):
        print("Livro criado")
        self.titulo = titulo
        self.autor = autor
        self.paginas = paginas

    def __str__(self):
        return "Título: %s , autor: %s, páginas: %s " \
               % (self.titulo, self.autor, self.paginas)

    def __len__(self):
        return self.paginas

    def len(self):
        return print("Páginas do livro com método comum: ", self.paginas)

livro1 = Livro("Os Lusíadas", "Luis de Camões", 8816)

# Métodos especiais
print(livro1)

# chamando __str__
print(str(livro1))

# chamando __len__
print(len(livro1))

# chamando metodo len e nao __len__
livro1.len()

# Ao executar a função del para remover um atributo, o Python executa:
# livro1.__delattr__("paginas")

# deletando atributo paginas do objeto livro1
del livro1.paginas

# verifica se o atributo paginas exista no objeto livro1
print(hasattr(livro1, "paginas"))