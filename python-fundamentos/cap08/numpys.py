# Para importar numpy, utilize: import numpy as np
# Você também pode utilizar: from numpy import * . Isso evitará a utilização de np.,
# mas este comando importará todos os módulos do NumPy.
# Para atualizar o NumPy, abra o prompt de comando e digite: pip install numpy -U

# Modulos Python para analise de dados chamado tb:
# Python Open Data Science Stack

# NumPy (Numerical Python) Matematica
# suporte para array e matrizes, alem de funcoes matematicas para esses objetos
# alta capacidade de processamento de arrays
# arrays e um conjunto de valores, todos do mesmo tipo e indexados por uma tupla de valores nao-negativos

# Importando o NumPy
import numpy as np

print('Versao: ' + np.__version__)


## CRIANDO ARRAYS ##

# Help
help(np.array)

# Array criado a partir de uma lista:
vetor1 = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8])

print(vetor1)

# Um objeto do tipo ndarray é um recipiente multidimensional de itens do mesmo tipo e tamanho.
print(type(vetor1))

# Usando métodos do array NumPy, soma acumulada de todos os objetos do array
print(vetor1.cumsum())

# Criando uma lista. Perceba como listas e arrays são objetos diferentes, com diferentes propriedades
lst = [0, 1, 2, 3, 4, 5, 6, 7, 8]

print(lst)

print(type(lst))

# Imprimindo na tela um elemento especifico no array
print(vetor1[0])

vetor1[0] = 100

print(vetor1)

# No array nao e possivel incluir elemento de outro tipo
# vetor1[0] = 'Novo elemento'

# Verificando o formato do array (9,) indica array unidimensional de tamanho 9
print(vetor1.shape)


## FUNCOES NUMPY ##
print('\n' + '## FUNCOES NUMPY ##' + '\n')

# A função arange cria um vetor contendo uma progressão aritmética a partir de um intervalo
# start, stop, step
vetor2 = np.arange(0., 4.5, .5)

print(vetor2)

# Verificando o tipo do objeto
print(type(vetor2))

# Formato do array, retorno (9,) Unidimensional
print(np.shape(vetor2))

# Tipo de dado que o array contem - float64
print(vetor2.dtype)

#  de 1 a 10, de 0.25 de diferenca entre cada valor
x = np.arange(1, 10, 0.25)
print(x)

# Cria um array preenchido com zeros, no caso 10 valores 0, serve para peso exemplo
print(np.zeros(10))


# Retorna 1 nas posicoes em diagonal e 0 no restante, cri uma matriz identidade
z = np.eye(3)
print(z)

# Os valores passados coo parametro, formam uma diagonal
d = np.diag(np.array([1, 2, 3, 4]))
print(d)

# Array de numeros complexos
c = np.array([1+2j, 3+4j, 5+6*1j])
print(c)

# Array de valores booleanos
b = np.array([True, False, False, True])
print(b)

# Array de strings
s = np.array(['Python', 'R', 'Julia'])
print(s)


# O método linspace (linearly spaced vector) retorna um número de
# valores igualmente distribuídos no intervalo especificado
print(np.linspace(0, 10))

print(np.linspace(0, 10, 15))

print(np.logspace(0, 5, 10))


## CRIANDO MATRIZES ##
print('\n' + '## CRIANDO MATRIZES ##' + '\n')

# Criando uma matriz
matriz = np.array([[1,2,3],[4,5,6]])
print(matriz)

# retorna a dimensao da matriz, (2,3) 2 linhas e 3 colunas
print('\n')
print(matriz.shape)

# Criando uma matriz 2x3 apenas com numeros 1
matriz1 = np.ones((2,3))
print('\n')
print(matriz1)

# Criando uma matriz a partir de uma lista de listas
lista = [[13, 81, 22], [0, 34, 59], [21, 48, 94]]

# A funcao matriz cria uma matriz a partir de uma sequencia
matriz2 = np.matrix(lista)
print('\n')
print(matriz2)

print('\n')
print(type(matriz2))

# Formato da matriz, retorno 3x3
print('\n')
print(np.shape(matriz2))

# Tamanho
print('\n')
print(matriz2.size)

# Tipo dos valores que o array contem, float, int, ...
print('\n')
print(matriz2.dtype)

# Verifica o tamanho de cada item na memoria
print('\n')
print(matriz2.itemsize)

# Verifica o tamanho total item na memoria
print('\n')
print(matriz2.nbytes)

# Alterando um elemento da matriz
matriz2[1,0] = 100
print('\n')
print(matriz2)


x = np.array([1, 2])  # NumPy decide o tipo dos dados
y = np.array([1.0, 2.0])  # NumPy decide o tipo dos dados
z = np.array([1, 2], dtype=np.float64)  # Forçamos um tipo de dado em particular

print('\n')
print (x.dtype, y.dtype, z.dtype)

# Criando matriz e forçando o tipo
matriz3 = np.array([[24, 76], [35, 89]], dtype=float)
print('\n')
print(matriz3)

# Tamanho
print('\n')
print(matriz3.itemsize)

# Verifica o tamanho total item na memoria
print('\n')
print(matriz3.nbytes)

# Verifica o numero de dimensoes do array
print('\n')
print(matriz3.ndim)

# Alterando um elemento da matriz
matriz3[1,1] = 100

print('\n')
print(matriz3)

## Usando o Método random() do NumPy ##
print('\n' + '## Usando o Método random() do NumPy ##' + '\n')

print(np.random.rand(10))


import matplotlib.pyplot as plt

# comando do jupiter, criar o grafico na mesma janela
# %matplotlib notebook

# Gerando grafico histograma a partir de dados rand
# plt.show((plt.hist(np.random.rand(1000))))

print('\n')
print(np.random.randn(5,5))

# Outro grafico
# plt.show(plt.hist(np.random.rand(1000)))


#imagem = np.random.rand(30, 30)
#plt.imshow(imagem, cmap = plt.cm.hot)
#plt.colorbar()

## Operações com datasets ##
print('\n' + '## Operações com datasets ##' + '\n')

import os
filename = os.path.join('iris.csv')

# No Windows use !more iris.csv. Mac ou Linux use !head iris.csv
# Comando head serve pra visualizar as primeiras linhas do arquivo
# !head iris.csv
#!more iris.csv

# Carregando um dataset para dentro de um array
# usecols = colunas que serao utilizadas
# skiprows = pula a linha que eu informar, no caso a 1
arquivo = np.loadtxt(filename, delimiter=',', usecols=(0, 1, 2, 3), skiprows=1)
print(arquivo)

print('\n')
print(type(arquivo))

# Gerando um plot a partir de um arquivo usando o numpy
var1, var2 = np.loadtxt(filename, delimiter=',', usecols=(0, 1), skiprows=1, unpack=True)
# plt.show(plt.plot(var1, var2, 'o', markersize=8, alpha=0.75))


## Estatística ##
print('\n' + '## Estatística ##' + '\n')

# Criando um array
A = np.array([15, 23, 63, 94, 75])


