# tecnica de software para extrair dados de paginas web
# sempre verificar se pode acessar os dados
# webscraping - conecta na pagina e salva em uma base para analises, unico objetivo
# webcrawler - acessa as paginas web e acompanha os hiperlinks, google usa webcrawler
# webcrawler acessa todos os links e indexas, como o google faz
# webcrawler motores de busca
# webcrawler informacoes genericas
# webscraping informacoes especificas

# verificar se a pagina html permite webscraping
# https://www.python.org/robots.txt
# acessar arquivo robots.txt
# User-agent: HTTrack
# User-agent: puf
# User-agent: MSIECrawler
# Disallow: / (não é permitido os agentes acima)
# The Krugle web crawler (though based on Nutch) is OK.
# User-agent: Krugle
# Allow: / (agente Krugle permitido acessar a partir do /)
# Disallow: /~guido/orlijn/ (agente Krugle não permitido acessar)
# Disallow: /webstats/ (agente Krugle não permitido acessar)
# No one should be crawling us with Nutch.
# User-agent: Nutch
# Disallow: / (agente Nutch não permitido)

# Hide old versions of the documentation and various large sets of files.
# User-agent: * (qualquer agente pode acessar o site. menos os enderecos abaixo)
# Disallow: /~guido/orlijn/
# Disallow: /webstats/

# Biblioteca usada para requisitar uma página de um web site
import urllib.request
from bs4 import BeautifulSoup

# Definimos a url
# Verifique as permissões em https://www.python.org/robots.txt
with urllib.request.urlopen("https://www.python.org") as url:
    page = url.read()

# Imprime o conteúdo (dados brutos)
# print(page)

# Analise o html na variável 'page' e armazene-o no formato Beautiful Soup
soup = BeautifulSoup(page, "html.parser")
#existem muito mais funcoes para capturar o html pela bibliote beatifulsoup
# captura o title da page html com a tag
print(soup.title)

# captura o title da page html somento com o texto
print(soup.title.string)

# retorna o primeiro link encontrado na pagina
print(soup.a)

# todos os links da pagina
print(soup.find_all("a"))

# vai buscar o conteudo da tag table na pagina html
tables = soup.find('table')
print(tables)