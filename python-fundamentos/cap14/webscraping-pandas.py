# Imports
import pandas as pd
import requests
from bs4 import BeautifulSoup 
from tabulate import tabulate

# URL
res = requests.get("http://www.nationmaster.com/country-info/stats/Media/Internet-users")

# Parser# Parse 
soup = BeautifulSoup(res.content,'lxml')

# Extrai a tabela do código HTML
table = soup.find_all('table')[0]

#print(table)

# Conversão da tabela HTML em um dataframe do Pandas
df = pd.read_html(str(table))

#print(df)
#[        #                                        COUNTRY         AMOUNT  DATE  \
#0       1                                          China    389 million  2009   
#1       2                                  United States    245 million  2009   
#2       3                                          Japan  99.18 million  2009   
#3     NaN    Group of 7 countries (G7) average (profile)  80.32 million  2009

# Conversão do dataframe para o formato JSON
#print(df[0].to_json(orient='records'))



# tabulando resultado com tabulate
# nada mais do que transformando em tabela
res =  requests.get("http://www.nationmaster.com/country-info/stats/Media/Internet-users")
soup = BeautifulSoup(res.content,'lxml')
table = soup.find_all('table')[0] 
df = pd.read_html(str(table))

print(tabulate(df[0], headers='keys', tablefmt='psql') )

# saida
#+-----+------+---------------------------------------------------+---------------+--------+---------+-----------+
#|     | #    | COUNTRY                                           | AMOUNT        |   DATE |   GRAPH |   HISTORY |
#|-----+------+---------------------------------------------------+---------------+--------+---------+-----------|
#|   0 | 1    | China                                             | 389 million   |   2009 |     nan |       nan |
#|   1 | 2    | United States                                     | 245 million   |   2009 |     nan |       nan |
#|   2 | 3    | Japan                                             | 99.18 million |   2009 |     nan |       nan |
#|   3 | nan  | Group of 7 countries (G7) average (profile)       | 80.32 million |   2009 |     nan |       nan |
