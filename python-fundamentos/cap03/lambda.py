# expressoes em lambda
# nos permitem criar funcoes anominas (funcoes in-line), isto quer dizer, que podemos criar funcoes
# ad-hoc sem a necessidade de definir uma funcao usando a palavra reservada def

# Passo a passo para transformar uma funcao em uma lambda (funcao simplificado em 1 linha de codigo)

# Definindo uma função - 3 linhas de código
def potencia(num):
    result = num**2
    return result

# Definindo uma função - 2 linhas de código
def potencia(num):
    return num**2

# Definindo uma função - 1 linha de código
def potencia(num): return num**2

# Definindo uma expressão lambda
potencia = lambda num: num**2

# Lembre: operadores de comparação retornam boolean, true or false
Par = lambda x: x%2==0
print(Par(3))
print(Par(4))

first = lambda s: s[0]
print(first('Python'))

reverso = lambda s: s[::-1]
print(reverso('Python'))

addNum = lambda x,y : x+y
print(addNum(2,3))