import math

# Definindo uma funcao
def primeiraFunc():
    print('Hello World')

primeiraFunc()

# Definindo uma funcao com parametro
def primeiraFunc(nome):
    print('Hello %s' %(nome))

primeiraFunc('Aluno')

def funcLeitura():
    for i in range (0, 5):
        print("Numero " + str(i))

funcLeitura()

# Funçao para somar numeros
def addNum(firstnum, secondnum):
    print("Primeiro numero: " + str(firstnum))
    print("Segundo numero: " + str(secondnum))
    print("Soma: ", firstnum + secondnum)

addNum(45, 3)

# Variavel global
var_global = 10 # Esta e uma variavel global

def multiply(num1, num2):
    # Variavel local
    var_local = num1 * num2
    print(var_local)

multiply(5, 25)

# Erro, variavel local na funcao acima, so pode usar dentr no escopo
# print(var_local)

# Valor absoluto
print(abs(-56))

print(abs(23))

# Boleando
print(bool(0)) # Saida False

print(bool(1)) # Saida True

# Erro ao executar por causa da conversão
#idade = input("Digite sua idade: ")
#if idade > 13:
    #print("Você pode acessar o Facebook")

# Usando a função int para converter o valor digitado
#idade = int(input("Digite sua idade: "))
#if idade > 13:
    #print("Você pode acessar o Facebook")

# string para int
print(int("26"))

# string para float
print(float("123.345"))

# string para int
print(str(14))

# tamanho
print(len([23,34,45,46]))


array = ['a', 'b', 'c']

print(max(array))
print(min(array))

list1 = [23, 23, 34, 45]

# somando lista
print(sum(list1))


def numPrimo(num):
    # Verificando se um número é primo.
    if (num % 2) == 0 and num > 2:
        return "Este número não é primo"
    for i in range(3, int(math.sqrt(num)) + 1, 2):
        if (num % i) == 0:
            return "Este número não é primo"
    return "Este número é primo"

print(numPrimo(541))


# Fazendo split dos dados
def split_string(text):
    return text.split(" ")

texto = "Esta função será bastante útil para separar grandes volumes de dados."

# Isso divide a string em uma lista.
print(split_string(texto))

# Podemos atribuir o output de uma função, para uma variável
token = split_string(texto)
print(token)

caixa_baixa = "Este Texto Deveria Estar Todo Em LowerCase"

def lowercase(text):
    return text.lower()

lowercased_string = lowercase(caixa_baixa)
print(lowercased_string)


# Funções com número variável de argumentos (argumentos dinamicos)
def printVarInfo(arg1, *vartuple):
    # Imprimindo o valor do primeiro argumento
    print("O parâmetro passado foi: ", arg1)

    # Imprimindo o valor do segundo argumento
    for item in vartuple:
        print("O parâmetro passado foi: ", item)
    return;

# Fazendo chamada à função usando apenas 1 argumento
printVarInfo(10)

printVarInfo('Chocolate', 'Morango', 'Banana')