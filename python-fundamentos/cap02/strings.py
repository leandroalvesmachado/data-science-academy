# Uma unica palavra
print('Oi')

# Uma frase
print('Criando uma string em python')

# Podemos usar aspas duplas
print("Podemos usar aspas duplas ou simples para strings em Python")

# Voce pode combinar aspas duplas e simples
print("Testando strings em 'Python'")

print("Testando \n strings \n em \n Python")

# Atribuindo uma string
s = 'Data Science Academy'
print(s)

# Primeiro elemento da string
print(s[0])

# Retorna todos os elementos da string, comecando pela posicao (Python comeca de 0)
print(s[1:])

# Retorna tudo ate a posicao 3
print(s[:3])

# Retorna tudo
print(s[:])

# Fatiando de 2 em 2 por palavra
print(s[::2])

# Fatiando de 1 em 1 por palavra inverso
print(s[::-1])

# Concatenando strings
s = s + 'melhor maneira de estar preparado'
print(s)

# Podemos usar o simbolo de multiplicacao para criar repeticao
letra = 'w'
print(letra * 3)

# Upper case
print(s.upper())

# Lower case
print(s.lower())

# Dividir uma string por espacos em branco (padrao)
print(s.split())

# Dividir uma string por um elemento
print(s.split('y'))

# Capitalize
s = 'seja bem vindo ao universo'
print(s.capitalize())

# Count elemento
s = 'seja bem vindo ao universo'
print(s.count('a'))

# Find posicao elemento
print(s.find('a'))

# E numero?
print(s.isalnum())

# Termina com o
print(s.endswith('o'))

# Comparando String
print("Python" == "R")

print("Python" == "Python")




