# Isso é uma lista# Isso
estudantes_lst = ["Mateus", 24, "Fernanda", 22, "Tamires", 26, "Cristiano", 25]
print(estudantes_lst)

# Isso é um dicionário
estudantes_dict = {"Mateus":24, "Fernanda":22, "Tamires":26, "Cristiano":25}
print(estudantes_dict)

print(estudantes_dict["Mateus"])

estudantes_dict["Pedro"] = 23
print(estudantes_dict["Pedro"])

print(estudantes_dict["Tamires"])

# zerando dicionario
estudantes_dict.clear();
print(estudantes_dict)

# deletando dicionario
del estudantes_dict
# print vai dar erro, pois nao existe mais, foi deletado
#print(estudantes_dict)


#criando dicionario
estudantes = {"Mateus":24, "Fernanda":22, "Tamires":26, "Cristiano":25}
print(estudantes)

# tamanho do dicionario, count
print(len(estudantes))

# imprimindo somente chaves, nao traz as chaves repetidas distinct
print(estudantes.keys())

# imprimindo somente os valores, nao traz os valores repetidos distinct
print(estudantes.values())

# retorno todos os itens, sem repeticao
print(estudantes.items())

estudantes2 = {"Maria":27, "Erika":28, "Milton":26}
print(estudantes2)

# atualizando o dicionario estudantes com o dicionario estudantes2, inseriando lista estudantes2 na estudantes
estudantes.update(estudantes2)
print(estudantes)

# criando dicionario vazio
dic1 = {}
print(dic1)

# criando indice no dicionario vazio
dic1["key_one"] = 2
print(dic1)

dic1[10] = 5
dic1[8.2] = "Python"
dic1["teste"] = 5


print(dic1)

# Dicionário de listas
dict3 = {'key1':1230,'key2':[22,453,73.4],'key3':['leite','maça','batata']}

print(dict3)

# Acessando um item da lista, dentro do dicionário
print(dict3['key3'][0].upper())

# Operações com itens da lista, dentro do dicionário
var1 = dict3['key2'][0] - 2
print(var1)


# Duas operações no mesmo comando, para atualizar um item dentro da lista# Duas o
dict3['key2'][0] -= 2
print(dict3)

# Criando dicionários aninhados
dict_aninhado = {'key1':{'key2_aninhada':{'key3_aninhada':'Dict aninhado em Python'}}}
print(dict_aninhado)

# navegando no niveis dos dicionarios aninhados
print(dict_aninhado['key1']['key2_aninhada']['key3_aninhada'])





