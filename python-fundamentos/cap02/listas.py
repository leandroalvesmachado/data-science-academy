# Criando uma lista com 1 unico elemento
listadomercado = ["ovos, farinha, leite, maças"]

print(listadomercado)

# Criando outra lista, mas com 4 elementos
listadomercado2 = ["ovos", "farinha", "leite", "maças"]

print(listadomercado2)

print(listadomercado[0])

print(listadomercado2[0])

# Criando lista
lista3 = [12, 100, "Universidade"]
print(lista3)

item1 = lista3[0]
item2 = lista3[1]
item3 = lista3[2]

print(item1, item2, item3)

# Imprimindo um item da lista
print(listadomercado2[2])

# Atualizando um item da lista
listadomercado2[2] = "chocolate"
print(listadomercado2[2])

# Error: out of index. Nao e possivel deletar um item que nao exista na lista
# del listadomercado2[4]

# Deletando um item especifico da lista
del listadomercado2[3]
print(listadomercado2)

# Criando uma lista de listas
listas = [[1,2,3], [10,15,14], [10.1,8.7,2.3]]
print(listas)

# Atribuindo um item da lista a uma variavel
a = listas[0]
print(a)

b = a[0]
print(b)

#Operações com listas

# Criando uma lista aninhada (lista de listas)
listas = [[1,2,3], [10,15,14], [10.1,8.7,2.3]]

# Atribuindo à variável a, o primeiro valor da primeira lista
a = listas[0][0]
print(a)

# Concatenando listas
lista_s1 = [34, 32, 56]
lista_s2 = [21, 90, 51]
lista_total = lista_s1 + lista_s2

print(lista_total)

# Criando uma lista
lista_teste_op = [100, 2, -5, 3.4]

# Verificando se o valor 10 pertence a lista
print(10 in lista_teste_op)

# Verificando se o valor 100 pertence a lista
print(100 in lista_teste_op)

# Função len() retorna o comprimento da lista
print(len(lista_teste_op))

# Função max() retorna o valor máximo da lista
print(max(lista_teste_op))

# Função min() retorna o valor mínimo da lista
print(min(lista_teste_op))

# Criando uma lista
listadomercado2 = ["ovos", "farinha", "leite", "maças"]
# Adicionando um item à lista
listadomercado2.append("carne")

print(listadomercado2)

listadomercado2.append("carne")

print(listadomercado2)

print(listadomercado2.count("carne"))

# Criando uma lista vazia
a = []
a.append(10)

print(a)

old_list = [1,2,5,10]
new_list = []

# Copiando os itens de uma lista para outra
for item in old_list:
    new_list.append(item)

print(new_list)


cidades = ['Recife', 'Manaus', 'Salvador']
cidades.extend(['Fortaleza', 'Palmas'])
print (cidades)

# Imprimindo indice do valor
print(cidades.index('Salvador'))

# Inserindo na posicao 2 o valor 10
cidades.insert(2, 110)
print(cidades)

# Remove um item da lista
cidades.remove(110)
print(cidades)

# Reverte a lista
cidades.reverse()
print(cidades)


x = [3, 4, 2, 1]

# Ordena a lista
x.sort()
print(x)





