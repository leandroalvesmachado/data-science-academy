# -*- coding: utf-8 -*-

# Atribuindo o valor 1 a variavel var_teste
var_teste = 1

# Imprimindo o valor da variavel
print(var_teste)

var_teste = 2

#int
print(type(var_teste))

var_teste = 9.5

#float
print(type(var_teste))

x = 1

print(x)

# Declaracao multipla

pessoa1, pessoa2, pessoa3 = "Maria", "Jose", "Tobias"

print(pessoa1)
print(pessoa2)
print(pessoa3)

fruta1 = fruta2 = fruta3 = "Laranja"

print(fruta1)
print(fruta2)
print(fruta3)

largura = 2
altura = 4
area = largura * altura

print(area)

# concatenacao de variaveis
nome = "Steve"
sobrenome = "Jobs"
fullname = nome + " " + sobrenome

print(fullname)

