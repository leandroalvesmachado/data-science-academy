import sqlite3
import random
import datetime
import matplotlib.pyplot as plt

# comando do jupiter, criar o grafico na mesma janela
# %matplotlib notebook


# Criando uma conexão
conn = sqlite3.connect('dsa.db')

# Criando um cursor
c = conn.cursor()

# Funcao para criar uma tabela
def create_table():
    c.execute('CREATE TABLE IF NOT EXISTS produtos('\
        'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'\
        'date TEXT,'\
        'prod_name TEXT,'\
        'valor REAL'\
    ')')

# Funcao para inserir uma linha
def data_insert():
    c.execute("INSERT INTO produtos VALUES(now, 'Teclado', 130)")
    conn.commit()
    c.close()
    conn.close()

# Usando variaveis para inserir dados
def data_insert_var():
    new_date = datetime.datetime.now()
    new_prod_name = 'monitor'
    new_valor = random.randrange(50, 100)
    c.execute("INSERT INTO produtos (date, prod_name, valor) VALUES (?, ?, ?)", (new_date, new_prod_name, new_valor))
    conn.commit()

# Leitura de dados
def leitura_todos_dados():
    c.execute("SELECT * FROM produtos")
    for linha in c.fetchall():
        print(linha)

# Leitura de registros especificos
def leitura_registros():
    c.execute("SELECT * FROM produtos WHERE valor > 60.0")
    for linha in c.fetchall():
        print(linha)

# Leitura de colunas especificas
def leitura_colunas():
    c.execute("SELECT * FROM produtos")
    for linha in c.fetchall():
        print(linha[3])

# Update
def atualiza_dados():
    c.execute("UPDATE produtos SET valor = 70.0 WHERE valor > 80.0")
    conn.commit()

# Delete
def remove_dados():
    c.execute("DELETE FROM produtos WHERE valor = 62.0")
    conn.commit()

# Gerar grafico com os dados no banco de dados
def dados_grafico():
    c.execute("SELECT id, valor FROM produtos")

    dados = c.fetchall()
    ids = []
    valores = []

    for linha in dados:
        ids.append(linha[0])
        valores.append(linha[1])

    plt.bar(ids, valores)
    plt.show()

# Gerando graficos
dados_grafico()