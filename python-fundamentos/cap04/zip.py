# zip usado para juntar listas

# pode ser usado em lista com quantidades diferentes
# mas o resultado sera o de menor quantidade de elementos
# uma lista com 3 e outra com 4, retorna uma lista com 3
# tupla ()
# lista []
# dicionario {}

# Criando duas listas
x = [1,2,3]
y = [4,5,6]

# Unindo as listas. Em Python3 retorna um iterator
print(zip(x, y))