# funcao filter recebe 2 argumentos (nome da funcao , uma sequencia Ex: lista)

# Criando uma função
def verificaPar(num):
    if num % 2 == 0:
        return True
    else:
        return False

# Chamando a função e passando um número como parâmetro. Retornará
# Falso de for ímpar e True se for par.
print(verificaPar(35))

lista = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
print(lista)

# somente filter retorna objeto da memoria
print(filter(verificaPar, lista))

# colocando uma list retorna a lista com o resultado
# no caso os numeros pares
print(list(filter(verificaPar, lista)))

# com lambda somente os pares
print(list(filter(lambda x: x%2==0, lista)))

# somente maiores que 8
print(list(filter(lambda num: num > 8, lista)))