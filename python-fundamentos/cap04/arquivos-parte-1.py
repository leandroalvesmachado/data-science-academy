# Metodo
# open() - usada para abrir o arquivo
# read() - leitura do arquivo
# write() - gravacao no arquivo
# seek() - retorna para o inicio do arquivo
# readlines() - retorna a lista de linhas do arquivo
# close() - fecha o arquivo

# Abrindo o arquivo para leitura
arq1 = open("arquivos/arquivo1.txt", "r")

# Lendo o arquivo
print(arq1.read())

# Contar o numero de caracteres
print(arq1.tell())

# Retornar para o inicio do arquivo
print(arq1.seek(0,0))

# Ler os primeiros 10 caracteres
print(arq1.read(10))


# Abrindo arquivo para gravacao
arq2 = open("arquivos/arquivo1.txt", "w")

# Como abrimos o arquivo para apenas gravacao, nao podemos usar comandos de leitura
# Erro, arquivo apenas para gravacao
# print(arq2.read())

# Gravando arquivo
arq2.write("Testando gravaçao de arquivos em python leandro")

# Fechando arquivo
arq2.close()

# Lendo arquivo gravado
arq2 = open("arquivos/arquivo1.txt", "r")
print(arq2.read())

# Acrescentando conteudo a = apende no final
arq2 = open("arquivos/arquivo1.txt", "a")
arq2.write(" Acrescentando conteudo leandro")
arq2.close()

arq2 = open("arquivos/arquivo1.txt", "r")
print(arq2.read())

# Retornando ao inicio do arquivo para leitura
arq2.seek(0,0)
print(arq2.read())


# Automatizando o processo de gravacao
fileName = input("Digite o nome do arquivo: ")
fileName = fileName + ".txt"

arq3 = open(fileName, "w")
arq3.write("Incluindo texto no arquivo criado")

