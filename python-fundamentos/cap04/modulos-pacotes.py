# Importando um módulo em Python
import math

# Modulos em python sao simplesmente arquivos python com a extensao .py, que implementam um
# conjunto de funcoes

# Podemos criar nosso proprio modulo e fazer o import, Ex: modulo para ler arquivos txt (reuso)

# import modulo
# ou
# import pacote.modulo (Pacote = um diretorio contendo modulos (.py) contendo um arquivo __init__.py

# repositorio de modulos feitos - https://pypi.org/

# Verificando todos os métodos disponíveis no módulo
print(dir(math))

# Usando um dos métodos do módulo math
print(math.sqrt(25))

# Usando o método
# Importando apenas um dos métodos do módulo math
from math import sqrt
print(sqrt(9))

# Help do método sqrt do módulo math
help(sqrt)

import random

print(random.choice(['Maça', 'Banana', 'Laranja']))

# gerando uma amostra
print(random.sample(range(100), 10))

# modulo pacote
import statistics

dados = [2.75, 1.75, 1.25, 0.25, 0.5, 1.25, 3.5]

# media
print(statistics.mean(dados))

# mediana
print(statistics.median(dados))

import os

# caminho /var/www/html/...
print(os.getcwd())


import sys

# parecido com o print
sys.stdout.write('Teste')

print(sys.version)


# Importando o módulo request do pacote urllib, usado para trazer url's
# para dentro do nosso ambiente Python
import urllib.request

# Variável resposta armazena o objeto de conexão à url passada como
# parâmetro
resposta = urllib.request.urlopen('http://python.org')

# Objeto resposta
print(resposta)

# Chamando o método read() do objeto resposta e armazenando o código
# html na variável html
html = resposta.read()

# Imprimindo html
print(html)
